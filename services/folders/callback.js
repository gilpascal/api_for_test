const Folders = require('./model')

module.exports = {

    GetAllFolder:async(req,res)=>{
        try {
            const folders = await Folders.find()
            res.status(200).json({
                status: true,
                response: folders
            })
        } catch (err) {
            res.status(500).json({
                status:false,
                error:err.message
            })
        }
    },

    CreateFolder:async(req,res)=>{
        const folder = new Folders({
            number_folder: req.body.number_folder,
            candidate_first_name: req.body.candidate_first_name,
            candidate_last_name: req.body.candidate_last_name,
            candidate_email: req.body.candidate_email,
            candidate_dob: req.body.candidate_dob,
            candidate_pob: req.body.candidate_pob,
            candidate_phone: req.body.candidate_phone,
            candidate_type_recruitment: req.body.candidate_type_recruitment,
            candidate_type_contract: req.body.candidate_type_contract,
            candidate_doc: req.body.candidate_doc,
            folder_status: req.body.folder_status,
            folder_emit: req.body.folder_emit
        })

        try {
            const newfolder = await folder.save()

            res.status(201).json({
                status: true,
                response: newfolder
            })
        } catch (error) {
            res.status(500).json({
                status: false,
                error: error.message
            })
        }
    },

    getOneFolder: async(req,res,next)=>{
        let folder

        try {

            folder = await Folders.findById(req.params.id)
            if(folder == null){
                res.status(404).json({
                    status: false,
                    response: "Cannot find this folder"
                })
            }else{
                res.status(200).json({
                    status: true,
                    response: folder
                })
            }
        } catch (err) {
            res.status(500).json({
                status: false,
                error: err.message
            })
        }
        res.folder = folder

        next()
        
    },

    updateFolder:async(req,res)=>{
        let folder
        if( req.body.candidate_first_name != null || req.body.candidate_last_name != null || req.body.candidate_email != null || req.body.candidate_dob != null || req.body.candidate_pob != null 
            || req.body.candidate_phone != null || req.body.candidate_type_recruitment != null || req.body.candidate_type_contract != null || req.body.candidate_doc != null){

                folder = new Folders({
                    /*number_folder: req.body.number_folder,*/
                    candidate_first_name: req.body.candidate_first_name,
                    candidate_last_name: req.body.candidate_last_name,
                    candidate_email: req.body.candidate_email,
                    candidate_dob: req.body.candidate_dob,
                    candidate_pob: req.body.candidate_pob,
                    candidate_phone: req.body.candidate_phone,
                    candidate_type_recruitment: req.body.candidate_type_recruitment,
                    candidate_type_contract: req.body.candidate_type_contract,
                    candidate_doc: req.body.candidate_doc,
                    folder_status: req.body.folder_status
                    
                })
                console.log(req.body.candidate_first_name, req.body.candidate_last_name, req.body.candidate_email, req.body.candidate_dob,
                    req.body.candidate_pob, req.body.candidate_phone, req.body.candidate_type_recruitment, req.body.candidate_type_contract, req.body.candidate_doc)
        }
        
        try {
            const folderupdating = await folder.save()
            res.status(200).json({
                status: true,
                response: folderupdating
            })
        } catch (err) {
            res.status(500).json({
                status: false,
                error: err.message
            })
        }
    },

    updateStatus: async(req,res)=>{

        let folds;
        if(req.body.folder_status != null){
            folds = new Folders({
                folder_status: req.body.folder_status
            })
        }

        try {
            const statusUpdated = await folds.save()
            res.status(200).json({
                status: true,
                response: statusUpdated

            })
        } catch (err) {
            res.status(500).json({
                status: false,
                error: err.message
            })
        }
        
    }

    

    


}
