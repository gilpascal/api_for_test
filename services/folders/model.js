const mongoose = require('mongoose')

const FolderSchema = mongoose.Schema({
    number_folder: {
        type: String,
        required: true
    },
    candidate_first_name: {
        type: String,
        required: true
    },
    candidate_last_name: {
        type: String,
        required: true
    },
    candidate_email: {
        type: String,
        required: true
    },
    candidate_dob: {
        type: String,
        required: true
    },

    candidate_pob: {
        type: String,
        required: true
    },
    candidate_phone: {
        type: Number ,
        required: true
    },
    candidate_type_recruitment: {
        type: String,
        required: true
    },

    candidate_type_contract: {
        type: String,
        required: true
    },

    candidate_doc: {
        type: Array,
        required: true
    },

    folder_status:{
        type: String,
        required: true
    },

    folder_emit:{
        type: String,
        required: true
    },

    created_at:{
        type: Date,
        required: true,
        default: Date.now
    }
   
})

module.exports = mongoose.model('Folders',FolderSchema)