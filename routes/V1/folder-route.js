const express = require('express')
const router = express.Router()
const { GetAllFolder, CreateFolder, getOneFolder, updateFolder, updateStatus} = require('../../services/folders/callback')

/**
 * Create one
 */
router.post("/", (req,res) =>{
    CreateFolder(req,res)
})


/**
 * Get all
 */
 router.get("/",  (req,res) =>{
    GetAllFolder(req,res)
})

/**
 * Get one
 */
router.get("/details/:id", getOneFolder, (req,res) =>{
    
})

/**
 * Patch one
 */
router.patch("/edit/:id", (req,res) =>{
    updateFolder(req,res)
})

/**
 * Patch one by status
 */
router.patch("/:id", (req,res) =>{
    updateStatus(req,res)
})


module.exports = router